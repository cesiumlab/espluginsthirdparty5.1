﻿#pragma once

 #if PLATFORM_WINDOWS
#ifdef ESTHIRDPARTYWRAP_EXPORTS
#define ESTHIRDPARTYWRAP_API __declspec(dllexport)
#else
#define ESTHIRDPARTYWRAP_API __declspec(dllimport)
#endif
#else
#define ESTHIRDPARTYWRAP_API __attribute__((visibility("default")))
#endif

#include <string>
#include <memory>
#include <vector> 
namespace EarthSDK
{
    class ESTHIRDPARTYWRAP_API CFoliageTile2
    {
    public:
        bool parse(std::string& bin);
    };

  	class ESTHIRDPARTYWRAP_API CFoliageTile
	{
	public:

        struct ESTHIRDPARTYWRAP_API Vector3F {
            float x = 0;
            float y = 0;
            float z = 0;
        };

        struct ESTHIRDPARTYWRAP_API Foliage{

            Vector3F position ;
            Vector3F scale ;
            Vector3F hpr ;
            float  height ;
            uint64_t xiaoban ;
            uint32_t id ;
        };

        struct ESTHIRDPARTYWRAP_API  FoliageType{

            std::string shuzhong;
            std::vector<Foliage> foliages; 
        };

        struct ESTHIRDPARTYWRAP_API Xiaoban {
            Vector3F position;
            std::string xiaoban;
            std::string you_shi_sz;
            std::string di_lei;
            std::string sen_lin_lb;
        };

        struct ESTHIRDPARTYWRAP_API   Poi{
            Vector3F position ;
            std::string id  ;
            std::string type ;
            std::string text ;
        };
 
		bool parse(std::string& data);
		double minHeight();
		double maxHeight();
		int64_t byteSize();

        std::vector<std::shared_ptr<FoliageType>> foliagetypes;
        std::vector<std::shared_ptr<Xiaoban>> xiaobans;
        std::vector<std::shared_ptr<Poi>> pois;
	private: 

      
        double _minHeight;
        double _maxHeight;
	};
}
