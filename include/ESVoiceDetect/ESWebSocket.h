#pragma once

#include <string>
#include <functional>
#include <memory>

#include <thread>
#include <condition_variable>
#include <mutex>  

#include "ESVoiceLib.h"
namespace XBSJ
{
	class ESWebSocketInternal;
	class ESVOICE_API ESWebSocket
	{
	public:
		typedef std::function<void(std::string  msg, std::string& data)> CallBack;

		ESWebSocket(CallBack   func);
		bool connect(std::string server);
		bool disconnect();
		bool send(std::string& bin);
		bool sendText(std::string& txt);

		bool isOpened() {
			return opened;
		}
	private:
		friend class ESWebSocketInternal;
		std::string server;
		CallBack callBack;
		std::shared_ptr< ESWebSocketInternal> _internal; 
		std::thread thread;

		std::mutex mutexSocketClosed;
		std::mutex mutexSocketOpened;
		std::condition_variable waitSocketClosed;
		std::condition_variable waitSocketOpened;

		void OnSocket(std::string  msg, std::string& data);
		bool opened = false;
	};

}
