#pragma once
#include <string> 
#include <functional>
#include <memory>
#include <vector>
#include <condition_variable>
#include <mutex>  
#include "ESVoiceLib.h"
namespace XBSJ
{
	class ESWebSocket;
	struct AudioBuffer;
	class ESVOICE_API ESVoiceDetect
	{
	public:
		
		typedef std::function<void(std::string type, std::string& msg)> CallBack;
		typedef std::function<void(std::string type, std::vector<std::string>& results)>  SentenceCallBack;
		ESVoiceDetect(CallBack  callback, SentenceCallBack scallback);
		 
		bool startRecord(std::string server);
		bool stopRecord();

		

		bool stopping = false;
		void sendVoiceData(uint8_t* buf, uint32_t size);
		void setWaveIn(void* waveIn);
	private:
 
		CallBack  callback;
		SentenceCallBack scallback;
		std::shared_ptr<ESWebSocket> socket;
	 
		std::shared_ptr<AudioBuffer> buffer1;
		std::shared_ptr<AudioBuffer> buffer2;

		bool startWaveIn();
		bool stopWaveIn();

		void* waveIn = nullptr;

		void onSocket(std::string  msg, std::string& data);

		
		bool sendStart(); 
		bool sendEnd();
		bool onSocketClosed();

		std::mutex mutexClose;
		std::condition_variable waitClose;

		void onSpeechEnd(); 
	};

}

