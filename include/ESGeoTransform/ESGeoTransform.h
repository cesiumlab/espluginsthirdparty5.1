﻿#pragma once
#include<string>

namespace XBSJ
{

	class ESGeoTransform
	{
	public:
		~ESGeoTransform();
		//初始化
		bool Init(std::string srs, std::string srsorigin, std::string outputsrs, std::string outputsrsorigin);
		//变换
		bool Transform(double& x, double& y, double& z);
		//是否初始化
		bool IsInited();
	private:
		void* _internal = nullptr;
	};
}
