#pragma once
#define XbsjRegister_Static
#define AUTH_USB
/* #undef AUTH_SERVER */
/* #undef HAS_SSL */

#include <string> 

#ifndef XbsjRegister_Static
#ifdef _WINDOWS
#ifdef  XBSJRegister_EXPORTS
#define XBSJRegister_API __declspec(dllexport)
#else
#define XBSJRegister_API __declspec(dllimport)
#endif
#else
#define XBSJRegister_API __attribute__((visibility("default")))
#endif
#else
#define XBSJRegister_API 
#endif

namespace XBSJ
{
	class XBSJRegister_API  Register
	{

	public:
		Register(std::string& param, uint64_t releasetime);
		~Register();
	//	static bool   Encrypt(std::string &key, std::string &content, std::string & output);
	//	static bool   Decrypt(std::string &key, std::string &content, std::string & output);
		
	//	static bool   GetMachineCode(std::string & param,std::string & output);

		//验证授权文件该软件的 使用权限
		//static bool   DefaultRegister(std::string  productID, std::string   licenfile);

		static bool   GetMachineCodeCesiumLab(std::string & code, std::string & info);
		//static bool   DecryptCesiumLab(std::string version, std::string licenfile, std::string & jsoncontent);
 
		static bool   GetMachineCodeOtherTool(std::string& code, std::string& info);
		//根据参数验证
		static bool   GetAuthContent(std::string &param, uint64_t releasetime, std::string & authcontent, std::string & errmsg);
		
		//根据参数和机器验证，releasetime为各工具编译时间，北京时间 时间戳，单位秒，获取方法如下
	    //std::string date = __DATE__;
		//std::string time = __TIME__;
		//uint64_t releasetime = Register::getTimestamp(date, time);
		static bool   GetAuthContent(std::string &param, uint64_t releasetime, std::string machine, std::string & authcontent, std::string& errmsg);
		//更新usb的授权文件
		static bool   updateUsbAuthfile(std::string & bindata, std::string file, std::string product);

		//获取usbkey
		static bool   GetUSBkey(std::string & usbkey);

		static std::string   string_To_UTF8(const std::string& str);

		//static bool   updateTimeUsbAuthfile(std::string file);
		//static bool   GetTimeUSBkey(std::string& usbkey);

		//带有需要付费功能的任务，需要定时检测授权，不检测在线授权，不更新授权信息
		bool   AuthTimer(std::string& param, uint64_t releasetime, std::string authcontent);

		//编译时间转时间戳
		static uint64_t getTimestamp(std::string releaseDate, std::string releaseTime);
		static bool DecryptTaskJson(const std::string& strBase64Json, std::string& strOut);

		static uint32_t UnAuthProductCount;
	};


	 bool   Encrypt(std::string &key, std::string &content, std::string & output);
	bool   Decrypt(std::string &key, std::string &content, std::string & output);
}
